#!/usr/bin/env python3

from _datetime import datetime, timedelta
import time
import re
import requests
import os

from bs4 import BeautifulSoup

# delete, list, update
# argparse
class CheckMangasUpdates:
    def __init__(self, update_file_path, fansub_file_path):
        self.update_file_path = update_file_path
        self.fansub_file_path = fansub_file_path

        self.parsed_file_data = None
        if os.path.exists(self.update_file_path):
            self.parsed_file_data = self.parse_file()

    def parse_file(self):
        with open(self.update_file_path, 'r') as checking_file:
            parsed_file_data = []
            for line in checking_file:
                splited_line = line.split("::")
                map_url_date = {"url": splited_line[0].strip(), "date": splited_line[1].strip()}
                parsed_file_data.append(map_url_date)
            return parsed_file_data

    def export_file(self):
        with open(self.update_file_path, 'w') as update_file:
            data = str()
            for manga in self.parsed_file_data:
                data += (manga['url'] + "::" + manga['date'] + "\n")

            update_file.write(data)

    def make_new_file(self):
        list_fansubs = open(self.fansub_file_path, 'r')

        try:
            for line in list_fansubs:
                date = self.last_update(line.strip())
                time.sleep(4)
                url_complete = line.strip() + "::" + date + "\n"
                create_file = open(self.update_file_path, 'a')
                create_file.write(url_complete)
                create_file.close()
        finally:
            list_fansubs.close()

    @staticmethod
    def parse_data(update_data, fansub_name):
        today = datetime.today()

        if any(["minutes" in update_data, "mins" in update_data]):
            return today.strftime("%Y-%m-%d")

        elif "hours" in update_data:
            return today.strftime("%Y-%m-%d")

        elif any(["day" in update_data, "days" in update_data]):
            date_words = update_data.split(" ")
            days_ago = int(date_words[0])
            date_n_days_ago = datetime.now() - timedelta(days=days_ago)

            return date_n_days_ago.strftime("%Y-%m-%d")
        else:
            if fansub_name == "mangarock":
                parse_update_data = datetime.strptime(update_data, "%b %d, %Y")
                return parse_update_data.strftime("%Y-%m-%d")
            elif fansub_name == "isekaiscan":
                parse_update_data = datetime.strptime(update_data, "%B %d, %Y")
                return parse_update_data.strftime("%Y-%m-%d")
            else:
                return update_data

    def last_update(self, url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 '
                          'Safari/537.36',
        }

        response = None
        try:
            response = requests.get(url, headers=headers)
        except requests.exceptions.RequestException as e:
            print(e)
            exit()

        soup = BeautifulSoup(response.text, "html.parser")

        if response.status_code == 503:
            all_href = soup.findAll('a', href=True)
            for content in all_href:
                if filter(lambda x: 'Cloudflare' in x, content.contents):
                    raise ValueError('Cloudflare protection activated on ' + url)

        update_data = (soup.find(self.search_tags(url)[0], {"class": self.search_tags(url)[1]})).text.strip()

        list_words = re.findall(r"\w+", url)
        fansub_name = list_words[1]

        return self.parse_data(update_data, fansub_name)

    @staticmethod
    def search_tags(url):
        list_words = re.findall(r"\w+", url)
        fansub_name = list_words[1]

        if fansub_name == "tmofans":
            return ['span', "badge badge-primary p-2"]
        if fansub_name == "mangarock":
            return ['td', "_2v_aG"]
        if fansub_name == "isekaiscan":
            return ['span', "chapter-release-date"]

    def check_updates(self):
        for manga in self.parsed_file_data:
            old_update = manga['date']
            try:
                new_update = self.last_update(manga['url']).strip()
                time.sleep(4)
            except Exception as error:
                print(error)
                continue

            if old_update == new_update:
                print(manga['url'], " has no updates")
            else:
                print(manga['url'], " has a new update!")
                manga['date'] = new_update


if __name__ == "__main__":
    mangasUpdate_ins = CheckMangasUpdates('/home/ondina/Develop/Python/WebScraping/updates.txt',
                                          '/home/ondina/Develop/Python/WebScraping/fansub.txt')
    mangasUpdate_ins.check_updates()
    mangasUpdate_ins.export_file()
